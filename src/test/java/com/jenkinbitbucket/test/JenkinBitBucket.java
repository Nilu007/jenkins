package com.jenkinbitbucket.test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class JenkinBitBucket {
	public class NewWebDriverTest {
		private WebDriver driver;		
		@Test				
		public void test() throws IOException {	
			driver.get("https://www.google.com/");  
			String title = driver.getTitle();				 
			Assert.assertTrue(title.contains("Google")); 
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("c:\\temp\\screenshot.png"));
		}	
		@BeforeTest
		public void beforeTest() {	
			System.setProperty("webdriver.chrome.driver","E:\\AUTOMATION TESTING\\TOOLS\\chromedriver_win32\\chromedriver.exe");
		    driver = new ChromeDriver(); 
		}
		
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}		
	}	

}
